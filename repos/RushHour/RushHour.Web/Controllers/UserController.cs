﻿using RushHour.Data.Entities;
using RushHour.Data.Repositories;
using RushHour.Data.Services;
using RushHour.Web.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using RushHour.Web.Mappings;
using RushHour.Web.App_Start;
using AutoMapper;
using RushHour.Web.ActionFilters;

namespace RushHour.Web.Controllers
{
    public class UserController : CrudController<User, UserViewModel>
    {
        private UnitOfWork unitOfWork = new UnitOfWork();



        public UserController(IService<User> service)
            : base(service)
        {

        }
        [IsAdmin]
        public override ActionResult Index()
        {

            return base.Index();
        }
        [IsAdmin]
        public override ActionResult LoadData()
        {
            return base.LoadData();
        }
        [IsAdmin]
        public override ActionResult Edit(int id)
        {

            return base.Edit(id);
        }

        public new ActionResult Edit(UserViewModel vModel, User model)
        {
            //OnBeforeEdit(vModel, model);
            //Mapper.Map(vModel, model);
            return View(model);
            //return base.Edit(vModel, model);
        }
        [IsAdmin]
        public override ActionResult Details(int id)
        {
            return base.Details(id);
        }
        [IsAdmin]
        public override ActionResult Delete(int id)
        {
            return base.Delete(id);
        }

        public override ActionResult DeleteConfirmed(int id)
        {
            return base.DeleteConfirmed(id);
        }
        //public override void OnBeforeEdit(UserViewModel vModel, User model)
        //{
        //    if (service.GetAll(s => s.Email == vModel.Email).Except(service.GetAll(s => s.Id == vModel.Id)).ToList().Count > 0)
        //    {
        //        ModelState.AddModelError("Email", "This email address already exist, please choose another.");
        //        return;
        //    }

        //}
    }
}