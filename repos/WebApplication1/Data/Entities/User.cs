﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Data.Entities
{
    class User : BaseEntity
    {
        public string FName { get; set; }
        public string LName { get; set; }
    }
}
