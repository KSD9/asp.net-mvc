﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Data.Entities
{
    class Books : BaseEntity

    {
        public string BookName { get; set; }
        public string Author { get; set; }
        public int Year { get; set; }
    }
}
