﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using ToDoApp.Data.Entities;

namespace ToDoApp.Data
{
    public class ToDoAppDBContext : DbContext
    {
        public ToDoAppDBContext()
            : base("ToDoAppDbContext")
        {
        }
        public IDbSet<Note> Notes { get; set; }
    }
}
