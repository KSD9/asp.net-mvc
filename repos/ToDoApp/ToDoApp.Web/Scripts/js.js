$(document).ready(function () {

    $('#NotesTable').dataTable({
        "ajax": {
            "url": "/Notes/LoadData",
            "type": "GET",
            "datatype": "json"
        },
        "columns": [
            { "data": "Title", "autoWidth": true },
            { "data": "Description", "autoWidth": true },
            { "data": "IsDone", "autoWidth": true },
        ]


    });
});