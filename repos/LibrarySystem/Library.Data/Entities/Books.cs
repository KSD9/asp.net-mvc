﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Library.Data.Entities
{
  public  class Books:BaseEntity
    {
        public string BookName { get; set; }
        public string Author { get; set; }
        public int Year { get; set; }
    }
}
