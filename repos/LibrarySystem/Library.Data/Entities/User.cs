﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Library.Data.Entities
{
  public  class User :BaseEntity

    {
        public string FName { get; set; }
        public string LName { get; set; }
        public int Age { get; set; }
    }
}
