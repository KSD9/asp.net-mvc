﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Library.Client.UserCreateViewModel;
using Library.Client.Repo;
using Library.Data.Entities;
using Library.Client.Configuration;


namespace Library.Client.Controllers
{
    public class UserController : Controller
    {
        // GET: User
        public ActionResult Index()
        {
            return View();
        }

        public ActionResult Create(UsersCreateView model) {

            if (!ModelState.IsValid)
            {
                return View(model);
            }

            User user = new User();
            user.FName = model.FName;
            user.LName = model.LName;
            user.Age = model.Age;
            var repository = new UserRepository(AppConfig.connectionString);
            repository.Insert(user);

            return RedirectToAction("Error");
        }
    }
}