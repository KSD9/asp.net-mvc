﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Library.Data.Entities;
using Library.DB;


namespace Library.Client.Repo
{
    public class UserRepository
    {
        private string connectionString;

        public UserRepository(string connectionString)
        {
            this.connectionString = connectionString;
        }

        


        public void Insert(User user)
        {

            IDbConnection connection = new SqlConnection(connectionString);

            using (connection)
            {

                try
                {
                    connection.Open();
                    IDbCommand command = connection.CreateCommand();
                    command.CommandText = @"INSERT INTO DataBase(FName,LName,Age)
                                          VALUES(@FName,@LName,@Age)";

                    IDataParameter parameter = command.CreateParameter();
                    parameter.ParameterName = "@FName";
                    parameter.Value = user.FName;
                    command.Parameters.Add(parameter);

                    parameter = command.CreateParameter();
                    parameter.ParameterName = "@LName";
                    parameter.Value = user.LName;
                    command.Parameters.Add(parameter);

                    parameter = command.CreateParameter();
                    parameter.ParameterName = "@Age";
                    parameter.Value = user.Age;
                    command.Parameters.Add(parameter);

                    command.ExecuteNonQuery();
                }
                catch (Exception)
                {

                    Console.WriteLine("Ënter Valid Data");
                }
            }
        }

    }
}