﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(Library.Client.Startup))]
namespace Library.Client
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
