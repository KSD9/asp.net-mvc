﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Web;


namespace Library.Client.Configuration
{
    public static class AppConfig
    {
        public static string connectionString { get; set; }

        static AppConfig()
        {
            connectionString = ConfigurationManager.ConnectionStrings["DataBase"].ToString();
        }
    }

}