﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;


namespace Library.Client.UserCreateViewModel
{
    public class UsersCreateView
    {
        [Required]
        public string FName { get; set; }

       
        public string LName { get; set; }

        public int Age { get; set; }

    }
}