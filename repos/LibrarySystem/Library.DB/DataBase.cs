﻿using System;
using System.Collections.Generic;
using System.Text;
using Library.Data.Entities;
using System.Data.Entity;

namespace Library.DB
{
  public  class DataBase : DbContext
    {
        public DataBase()
            :base ("name=DataBase")
        {
            Database.SetInitializer<DataBase>(new CreateDatabaseIfNotExists<DataBase>());
        }
        public virtual DbSet<User> Users { get; set; }
        public virtual DbSet<Books> Books { get; set; }
    }
}
